FROM node:8.17.0-alpine

RUN apk add --no-cache tini

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install 

# Bundle app source
COPY . .

#ENV DEBUG=*

RUN ./node_modules/.bin/ng build
ENTRYPOINT ["tini", "--"]
CMD ["node", "server.js"]
EXPOSE 4200

