import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnInit, OnDestroy {

  private unsubscribe = new Subject<void>();
  
  public viewers: number = 0;
  public liveString = 'Go Live!';
  private suspendToggle: boolean = false;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.messages
      .takeUntil(this.unsubscribe)
      .subscribe(msg => {
        console.log('chat message =>', msg);
        switch (msg.type) {
          case 'viewers':
            this.viewers = (this.viewers + msg.data.amount > 0) ? this.viewers += msg.data.amount : 0;
            break;
          case 'golive':
            this.liveString = (msg.data.live) ? 'LIVE' : 'Go Live!';
            this.chatService.liveOn = (msg.data.live) ? true : false;
            break;
          case 'reset':
            this.resetAll();
            break;
        }
      });
  }


  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }


  public toggleLive() {
    this.liveString = (this.chatService.liveOn) ? 'Go Live!' : 'LIVE';
    this.chatService.toggleLive();
  }


  private resetAll() {
    this.viewers = 0;
    this.liveString = 'Go Live!';
  }

}
