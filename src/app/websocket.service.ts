import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
// import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { Observable, Subject } from 'rxjs/Rx';

@Injectable()
export class WebsocketService {

  private socket;

  constructor() { }


  public connect(): Subject<MessageEvent> {
    this.socket = io(environment.ws_url);

    let observable = new Observable(observer => {
      // Websocket message received
      this.socket.on('message', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      }
    });

    let observer = {
      next: (data: any) => {
        console.log('socket emit =>', data);
        this.socket.emit('message', data);
      }
    };

    return Subject.create(observer, observable);
  }

}
