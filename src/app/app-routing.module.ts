import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ControlComponent } from './control/control.component';
import { YeehawComponent } from './yeehaw/yeehaw.component';
import { LiveComponent } from './live/live.component';

const routes: Routes = [
  { path: '', component: YeehawComponent},
  { path: 'control', component: ControlComponent },
  { path: 'live', component: LiveComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}