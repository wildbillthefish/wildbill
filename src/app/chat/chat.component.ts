import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { ChatService } from '../chat.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {

  @ViewChild('messageContainer') messageContainer: ElementRef;

  private unsubscribe = new Subject<void>();
  private jumpButton: any;

  public chatMessages = [];
  public tipJar: number = 0;

  constructor(
    private chatService: ChatService,
    private scrollToService: ScrollToService
  ) {}


  ngOnInit() {
    this.chatService.messages
      .takeUntil(this.unsubscribe)
      .subscribe(msg => {
        console.log('chat message =>', msg);
        switch (msg.type) {
          case 'chat':
            this.chatMessages.push(msg.data);
            this.triggerScrollTo();
            break;
          case 'tip':
            this.tipJar += msg.data.amount;
            break;
          case 'reset':
            this.resetAll();
        }
      });

    this.jumpButton = this.messageContainer.nativeElement.children.jump;

    this.messageContainer.nativeElement.onscroll = () => {
      if ( (this.messageContainer.nativeElement.scrollTop + this.messageContainer.nativeElement.offsetHeight) >= this.messageContainer.nativeElement.scrollHeight - 50 ) {
        this.jumpButton.style.display = 'none';
      } else {
        this.jumpButton.style.display = 'block';
      }
    };

    console.log('messageContainer =>', this.messageContainer);
    console.log('jumpButton =>', this.jumpButton);
  }


  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }


  private triggerScrollTo() {
    const config: ScrollToConfigOptions = {
      target: 'bottom',
      duration: 0
    };
    this.scrollToService.scrollTo(config);
  }


  private resetAll() {
    this.chatMessages = [];
    this.tipJar = 0;
    this.jumpButton.style.display = 'none';
  }

}
