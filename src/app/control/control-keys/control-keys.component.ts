import { Component, OnInit, HostListener } from '@angular/core';
import { ChatService } from '../../chat.service';

let shiftKeyPressed = false;
// Keycode mapping
const SHIFT_KEY = 16;
const NEXT_MESSAGE_KEY = 190; // '>' key
const BLACKOUT_KEY = 81; // Q key
const LIVE_KEY = 76; // L key
const VIEWERS_1_KEY = 90; // Z key
const VIEWERS_2_KEY = 88; // X key
const VIEWERS_5_KEY = 67; // C key
const VIEWERS_10_KEY = 86; // V key
const VIEWERS_100_KEY = 66; // B key
const TIP_ADD_1_KEY = 49; // 1 key
const TIP_ADD_2_KEY = 50; // 2 key
const TIP_ADD_5_KEY = 51; // 3 key
const TIP_ADD_10_KEY = 52; // 4 key
const TIP_ADD_20_KEY = 53; // 5 key
const TIP_ADD_40_KEY = 54; // 6 key
const TIP_ADD_60_KEY = 55; // 7 key
const TIP_ADD_100_KEY = 56; // 8 key
const SILENT_TIP_KEY = 57; // 9 key
const SPITOON_SOUND_KEY = 187; // '=' key

@Component({
  selector: 'app-control-keys',
  templateUrl: './control-keys.component.html',
  styleUrls: ['./control-keys.component.css']
})
export class ControlKeysComponent implements OnInit {

  constructor(private chatService: ChatService) { }

  ngOnInit() {
  }


  @HostListener('window:keydown', ['$event'])
  onKeydown(event) {

    // Key press actions
    switch(event.keyCode) {
      case SHIFT_KEY:
          // Activate shift state
          shiftKeyPressed = true;
          break;
      case NEXT_MESSAGE_KEY:
          // trigger next chat message
          this.nextMessage();
          break;
      case BLACKOUT_KEY:
          if (shiftKeyPressed) {
            this.toggleBlackout();
          }
          break;
      case LIVE_KEY:
          if (shiftKeyPressed) {
            this.toggleLive();
          }
          break;
      case VIEWERS_1_KEY:
          this.addViewers(shiftKeyPressed ? -1 : 1);
          break;
      case VIEWERS_2_KEY:
          this.addViewers(shiftKeyPressed ? -2 : 2);
          break;
      case VIEWERS_5_KEY:
          this.addViewers(shiftKeyPressed ? -5 : 5);
          break;
      case VIEWERS_10_KEY:
          this.addViewers(shiftKeyPressed ? -10 : 10);
          break;
      case VIEWERS_100_KEY:
          this.addViewers(shiftKeyPressed ? -100 : 100);
          break;
      case TIP_ADD_1_KEY:
          this.addTip(1);
          break;
      case TIP_ADD_2_KEY:
          this.addTip(2);
          break;
      case TIP_ADD_5_KEY:
          this.addTip(5);
          break;
      case TIP_ADD_10_KEY:
          this.addTip(10);
          break;
      case TIP_ADD_20_KEY:
          this.addTip(20);
          break;
      case TIP_ADD_40_KEY:
          this.addTip(40);
          break;
      case TIP_ADD_60_KEY:
          this.addTip(60);
          break;
      case TIP_ADD_100_KEY:
          this.addTip(100);
          break;
      case SILENT_TIP_KEY:
          this.silentTip();
          break;
      case SPITOON_SOUND_KEY:
          this.spitoonSound();
          break;
    }
  }


  @HostListener('window:keyup', ['$event'])
  onKeyup(event) {
      if (event.keyCode === SHIFT_KEY) {
          shiftKeyPressed = false;
      }
  }


  private nextMessage() {
    this.chatService.nextMessage();
  }


  private addViewers(amount: number) {
    console.log('add viewers');
    this.chatService.addViewers(amount);
  }

  
  private addTip(amount: number) {
    console.log('add tip');
    this.chatService.addTip(amount);
  }


  private silentTip() {
    this.chatService.silentTip();
  }


  public spitoonSound() {
    this.chatService.spitoonSound();
  }


  private toggleBlackout() {
    this.chatService.toggleBlackout();
  }

  public toggleLive() {
    this.chatService.toggleLive();
  }

}
