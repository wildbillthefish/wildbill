# WildBill

## Pre-requisites
(Check new computers to see that these are met)

Have [Node 8.x (aka "Carbon")](https://nodejs.org/dist/latest-v8.x/) installed **OR** have [NVM](https://github.com/nvm-sh/nvm#installing-and-updating) installed. If `nvm` is used, either set `carbon` as your default (run `nvm alias default carbon`) or run `nvm use` from the wildbill directory every time before following any of the other instructions on this page.

## Install for the first time

`git clone https://gitlab.com/wildbillthefish/wildbill.git`

This will create a wildbill directory containing the code.

`npm i` will install the dependencies.

## Update the code

`git pull` from the wildbill directory. 

This will update the code. Run `npm i` again to ensure that no dependencies have changed.

## Running the server ("LIVE" mode)

`npm start` from the wildbill directory will launch the server. This will launch the server for both the control and the YerToobz page. You can access these at `http://localhost:4200/control` and `http://localhost:4200/`, respectively. There is also a script called WILD-BILL-LIVE which does nothing more than launch the server and open the two pages in a browser.


## Developers

The rest of the information in this document is intended for developers.


### Development server

`ng serve` will launch a development server and give you the URL at which it is running. This is normally `http://localhost:4200/`.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `npm e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.
